## 版权声明

light-frame-rpc使用了一些第三方开源组件，其中主要依赖列举如下：

    Spring under Apache 2.0 license
    Spring Boot under Apache 2.0 license
    Netty under the Apache 2.0 license
    Zkclient under the Apache 2.0 license
    SLF4j under the MIT License
    
