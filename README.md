## light-frame-rpc

![license](https://img.shields.io/badge/license-Apache--2.0-green.svg)

light-frame-rpc是一款轻量级的分布式服务框架，致力于简化应用之间的 RPC 调用，为应用提供稳定高效的远程服务调用方案。

![架构设计](https://gitee.com/cdlvyh/light-frame-rpc/raw/master/doc/image/architecture_design.png)


## 一、功能简介

light-frame-rpc是一款分布式服务框架，主要提供以下能力：

- 透明化、高性能的远程服务调用
- 支持多种注册中心的集成
- 支持 hessian、hessian1等序列化框架
- 支持 LoadBalance 多种服务路由及负载均衡策略
- 支持 Filter 过滤器实现AOP功能
- 支持 Cluster 多种集群容错方案
- 支持 Eventbus 事件总线异步消息处理功能
- 支持同步、单向、回调、泛化等多种调用方式
- 支持泛化调用，消费者应用不依赖服务方提供的API
- 支持SPI扩展，提供灵活可插拔式组件扩展

## 二、快速开始

请查看项目文档中[快速开始](https://gitee.com/cdlvyh/light-frame-rpc/blob/master/quickstart.md)来了解如何快速上手使用light-frame-rpc。

## 三、如何贡献

light-frame-rpc欢迎广大开发者创建Pull Request来贡献代码，代码通过审核后会被合并到master主分支。

light-frame-rpc编译环境要求为JDK8或以上版本，需要采用 [Apache Maven 3.5.0](https://archive.apache.org/dist/maven/maven-3/3.5.0/binaries/) 或者更高的版本进行编译。

## 四、感谢

light-frame-rpc系统部分借鉴参考了XXL-RPC和SOFA-RPC等分布式RPC服务框架整体架构设计思路，在此表示感谢。

## 五、示例

light-frame-rpc的示例工程，详见light-frame-rpc-sample。

## 六、开源许可

light-frame-rpc基于Apache License 2.0协议，light-frame-rpc依赖了一些第三方组件，其开源协议参见[依赖组件版权说明](https://gitee.com/cdlvyh/light-frame-rpc/blob/master/NOTICE.md)。
