package com.lvyh.lightframe.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PullRequest implements Serializable {
    private String consumerId;

    private String topic;

    private String queueId;

    private String consumerGroup;

    private long nextOffset;

    private int consumeMessageBatchSize = 1;
}
