/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.provider.config;

import com.lvyh.lightframe.common.constant.RpcConstants;
import com.lvyh.lightframe.core.config.RegistryConfig;
import com.lvyh.lightframe.core.config.ServerConfig;
import com.lvyh.lightframe.core.provider.transport.ProviderBootstrapManager;
import com.lvyh.lightframe.core.provider.spring.DynamicProviderBeanRegister;
import com.lvyh.lightframe.common.util.IpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

import java.util.Arrays;
/**
 * @author lvyh 2021/05/06.
 */
@Configuration
@Import(DynamicProviderBeanRegister.class)
public class ApplicationConfig implements EnvironmentAware {
    private Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);

    private Environment environment;

    @Bean
    public ServerConfig serverConfig() {
        ServerConfig serverConfig = new ServerConfig();
        serverConfig.setName(environment.getProperty("rpc.provider.application.name"));
        serverConfig.setPort(Integer.parseInt(environment.getProperty("rpc.provider.protocol.port")));
        serverConfig.setIp(IpUtil.getIpAddress());
        serverConfig.setFilter(Arrays.asList("tps", "echo", "trace"));
        serverConfig.setSerializer(RpcConstants.SERIALIZE_HESSIAN);
        serverConfig.setTransport(RpcConstants.TRANSPORT_NETTY);
        return serverConfig;
    }

    @Bean
    public RegistryConfig registryConfig() {
        String zkAddress = environment.getProperty("rpc.provider.registry.zkaddress");
        RegistryConfig registry = new RegistryConfig();
        registry.setZkAddress(zkAddress);
        registry.setRegistry(RpcConstants.REGISTRY_TYPE_ZK);
        return registry;
    }

    @Bean
    public ProviderBootstrapManager providerBootstrapManager() {
        ProviderBootstrapManager providerBootstrapManager = new ProviderBootstrapManager();
        logger.info("[ProviderBootstrapManager] init completed.");
        return providerBootstrapManager;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

}