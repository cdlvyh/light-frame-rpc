/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.provider.service;

import com.lvyh.lightframe.api.OrderInfoService;
import com.lvyh.lightframe.api.dto.OrderInfoRequest;
import com.lvyh.lightframe.api.dto.OrderInfoResponse;
import com.lvyh.lightframe.core.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lvyh 2021/05/06.
 */
@Service(OrderInfoService.class)
@org.springframework.stereotype.Service
public class OrderInfoServiceImpl implements OrderInfoService {
    private static Logger logger = LoggerFactory.getLogger(OrderInfoService.class);

    @Override
    public OrderInfoResponse queryOrderInfo(OrderInfoRequest request) {
        OrderInfoResponse response = new OrderInfoResponse();
        response.setAmount("260");
        response.setOrderId("111111111111");
        response.setProductId(500000);
        response.setUserId(request.getUserId());
        logger.info("queryOrderInfo request:{}, result:{}", request, response);
        return response;
    }

}
