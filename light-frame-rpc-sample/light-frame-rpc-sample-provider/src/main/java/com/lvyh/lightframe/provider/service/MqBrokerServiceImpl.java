/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.provider.service;

import com.lvyh.lightframe.api.MqBrokerService;
import com.lvyh.lightframe.api.OrderInfoService;
import com.lvyh.lightframe.api.dto.MqMessage;
import com.lvyh.lightframe.api.dto.PullRequest;
import com.lvyh.lightframe.api.dto.PullResult;
import com.lvyh.lightframe.core.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lvyh 2021/05/06.
 */
@Service(MqBrokerService.class)
@org.springframework.stereotype.Service
public class MqBrokerServiceImpl implements MqBrokerService {
    private static Logger logger = LoggerFactory.getLogger(MqBrokerServiceImpl.class);

    @Override
    public PullResult pullMessage(PullRequest pullRequest) {
        PullResult response = new PullResult();
        MqMessage message = new MqMessage();
        message.setMsgId("123");
        response.setMessage(message);
        return response;
    }

}
