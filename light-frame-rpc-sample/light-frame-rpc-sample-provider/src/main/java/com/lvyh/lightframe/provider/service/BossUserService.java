/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.provider.service;

import com.lvyh.lightframe.core.annotation.Service;
import com.lvyh.lightframe.provider.dto.UserInfoRequest;
import com.lvyh.lightframe.provider.dto.UserInfoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
/**
 * @author lvyh 2021/05/06.
 */
@Service(BossUserService.class)
@org.springframework.stereotype.Service
public class BossUserService {
    private static Logger logger = LoggerFactory.getLogger(BossUserService.class);

    public UserInfoResponse getUserInfo(UserInfoRequest param) {

        UserInfoResponse result = new UserInfoResponse();
        BeanUtils.copyProperties(param, result);
        result.setRtnMsg("ok");
        result.setStatus(200);
        logger.info("getUserInfo request:{}, result:{}", param, result);
        return result;
    }

}
