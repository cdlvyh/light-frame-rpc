/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.consumer.controller;

import com.lvyh.lightframe.api.MqBrokerService;
import com.lvyh.lightframe.api.OrderInfoService;
import com.lvyh.lightframe.api.dto.OrderInfoRequest;
import com.lvyh.lightframe.api.dto.OrderInfoResponse;
import com.lvyh.lightframe.api.dto.PullRequest;
import com.lvyh.lightframe.api.dto.PullResult;
import com.lvyh.lightframe.common.constant.RpcConstants;
import com.lvyh.lightframe.core.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Normal invoke
 */
@Controller
public class IndexController {

    @Reference(timeout = 10000)
    private OrderInfoService orderInfoService;

    @Reference(timeout = 10000)
    private MqBrokerService mqBrokerService;

    //http://localhost:8081/pull-message
    @RequestMapping("pull-message")
    @ResponseBody
    public PullResult pullMessage() throws Exception {

        PullRequest request = new PullRequest();
        request.setQueueId("1");
        PullResult response = mqBrokerService.pullMessage(request);
        return response;
    }

    //http://localhost:8081/query-order-info?userId=3000
    @RequestMapping("query-order-info")
    @ResponseBody
    public OrderInfoResponse queryOrderInfo(String userId) throws Exception {

        OrderInfoRequest request = new OrderInfoRequest();
        request.setUserId(Long.parseLong(userId));
        OrderInfoResponse response = orderInfoService.queryOrderInfo(request);
        return response;
    }

}
