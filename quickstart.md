# light-frame-rpc 快速入门

本文档演示了如何应用 light-frame-rpc 进行服务的发布和引用。 

本实例将在本地模拟服务端启动监听一个端口并发布一个服务，客户端引用该服务进行RPC调用。

注册中心默认启用zookeeper，本地需预先启动zk，可以参考当前试验zk版本及命令如下：

```
$ cd c:\softwork\zookeeper-3.4.13\bin

c:\softwork\zookeeper-3.4.13\bin

$ zkServer.cmd
```

以下仅演示普通rpc调用场景，对于泛化调用，请参考：light-frame-rpc-sample-consumer中GenericController实例。

## 1、定义 RPC 接口

> 参见 light-frame-rpc-sample-api 模块

```
package com.lvyh.lightframe.api;

public interface OrderInfoService {

    OrderInfoResponse queryOrderInfo(OrderInfoRequest request);

}
```

需要将 RPC 接口与 RPC 实现分别存放在不同的模块中。

## 2、发布 RPC 服务

> 参见 light-frame-rpc-sample-provider 模块

### 2.1 引入 Maven 依赖

```
<dependency>
    <groupId>com.lvyh.lightframe</groupId>
    <artifactId>light-frame-rpc-core</artifactId>
    <version>${project.parent.version}</version>
</dependency>

<dependency>
    <groupId>com.lvyh.lightframe</groupId>
    <artifactId>light-frame-rpc-sample-api</artifactId>
    <version>${project.parent.version}</version>
</dependency>

```

- RPC Sample API：RPC 接口依赖
- RPC Provider：RPC 服务提供者相关依赖


### 2.2 实现 RPC 接口

```
@Service(OrderInfoService.class)
@org.springframework.stereotype.Service
public class OrderInfoServiceImpl implements OrderInfoService {
    private static Logger logger = LoggerFactory.getLogger(OrderInfoService.class);

    @Override
    public OrderInfoResponse queryOrderInfo(OrderInfoRequest request) {
        OrderInfoResponse response = new OrderInfoResponse();
        response.setAmount("260");
        response.setOrderId("111111111111");
        response.setProductId(500000);
        response.setUserId(request.getUserId());
        logger.info("queryOrderInfo request:{}, result:{}", request, response);
        return response;
    }

}
```

- 必须在 @Service 注解中指定 RPC 接口

### 2.3 配置 RPC 服务端

```
@Configuration
@Import(DynamicProviderBeanRegister.class)
public class ApplicationConfig implements EnvironmentAware {
    private Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);

    private Environment environment;

    @Bean
    public ServerConfig serverConfig() {
        ServerConfig serverConfig = new ServerConfig();
        serverConfig.setName(environment.getProperty("rpc.provider.application.name"));
        serverConfig.setPort(Integer.parseInt(environment.getProperty("rpc.provider.protocol.port")));
        serverConfig.setIp(IpUtil.getIpAddress());
        serverConfig.setFilter(Arrays.asList("tps", "echo", "trace"));
        serverConfig.setSerializer(RpcConstants.SERIALIZE_HESSIAN);
        serverConfig.setTransport(RpcConstants.TRANSPORT_NETTY);
        return serverConfig;
    }

    @Bean
    public RegistryConfig registryConfig() {
        String zkAddress = environment.getProperty("rpc.provider.registry.zkaddress");
        RegistryConfig registry = new RegistryConfig();
        registry.setZkAddress(zkAddress);
        registry.setRegistry(RpcConstants.REGISTRY_TYPE_ZK);
        return registry;
    }

    @Bean
    public ProviderBootstrapManager providerBootstrapManager() {
        ProviderBootstrapManager providerBootstrapManager = new ProviderBootstrapManager();
        logger.info("[ProviderBootstrapManager] init completed.");
        return providerBootstrapManager;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

}

```

- RegistryConfig：用于服务注册
- ServerConfig：用于发布 RPC 服务，需要提供服务器端口

注册到 ZooKeeper 中的 ZNode 路径为：`registry/service/address`.

### 2.4 启动 RPC 服务

```
package com.lvyh.lightframe.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author lvyh 2021/05/06.
 */
@SpringBootApplication
public class ProviderApplication {

	public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class, args);
	}

}
```

运行 ProviderApplication 类，将对外发布 RPC 服务，同时进行服务注册。


## 3、调用 RPC 服务

> 参见 light-frame-rpc-sample-consumer 模块

### 3.1 添加 Maven 依赖

```
<dependency>
    <groupId>com.lvyh.lightframe</groupId>
    <artifactId>light-frame-rpc-core</artifactId>
    <version>${project.parent.version}</version>
</dependency>

<dependency>
    <groupId>com.lvyh.lightframe</groupId>
    <artifactId>light-frame-rpc-sample-api</artifactId>
    <version>${project.parent.version}</version>
</dependency>

```

- RPC Sample API：RPC 接口所在模块的依赖
- RPC Consumer：RPC 消费者框架的依赖

### 3.2 配置 RPC 客户端

```
@Configuration
@Import(DynamicConsumerBeanRegister.class)
public class ApplicationConfig implements EnvironmentAware {
    private Logger logger = LoggerFactory.getLogger(ApplicationConfig.class);

    private Environment environment;

    @Bean
    public ConsumerConfig consumerConfig() {
        ConsumerConfig consumerConfig = new ConsumerConfig();
        consumerConfig.setName(environment.getProperty("rpc.consumer.application.name"));
        consumerConfig.setIp(IpUtil.getIpAddress());

        consumerConfig.setFilter(Arrays.asList("tps", "consumerGeneric"));
        consumerConfig.setTimeout(2000);
        consumerConfig.setInvokeType(RpcConstants.INVOKER_TYPE_SYNC);
        consumerConfig.setLoadBalance(RpcConstants.LOADBALANCE_RANDOM);
        consumerConfig.setSerializer(RpcConstants.SERIALIZE_HESSIAN);
        consumerConfig.setTransport(RpcConstants.TRANSPORT_CLIENT_NETTY);
        return consumerConfig;
    }

    @Bean
    public RegistryConfig registryConfig() {
        String zkAddress = environment.getProperty("rpc.consumer.registry.zkaddress");
        RegistryConfig registry = new RegistryConfig();
        registry.setZkAddress(zkAddress);
        registry.setRegistry(RpcConstants.REGISTRY_TYPE_ZK);
        return registry;
    }

    @Bean
    public ConsumerInvokeManager consumerInvokeManager() {
        ConsumerInvokeManager consumerInvokeManager = new ConsumerInvokeManager();
        logger.info("[ConsumerInvokeManager] init completed.");
        return consumerInvokeManager;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

```

- RegistryConfig：用于服务发现
- ConsumerConfig：用于配置消费者路由、调用方式、超时等元信息
- ConsumerInvokeManager：用于获取 RPC 代理接口


### 3.3 调用 RPC 服务

```
@Controller
public class IndexController {

    @Reference(timeout = 10000)
    private OrderInfoService orderInfoService;

    @RequestMapping("query-order-info")
    @ResponseBody
    public OrderInfoResponse queryOrderInfo(String userId) throws Exception {

        OrderInfoRequest request = new OrderInfoRequest();
        request.setUserId(Long.parseLong(userId));
        OrderInfoResponse response = orderInfoService.queryOrderInfo(request);
        return response;
    }

}

```

### 3.4 启动 RPC 消费者

```
package com.lvyh.lightframe.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author lvyh 2021/05/06.
 */
@SpringBootApplication
public class ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

}
```

### 3.5 测试运行接口

浏览器输入以下测试地址：
```
http://localhost:8081/query-order-info?userId=3000
```


服务端将打印：
```
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-1581662988] INFO  - [TpsLimitFilter] start to execute tps limit filter.
2021-05-31 15:31:04 [nioEventLoopGroup-3-1] INFO  - [NettyServerHandler] server receive request, param:{"attachment":{"traceId":"23640027-9505-430c-bce7-4414fb34debf"},"className":"com.lvyh.lightframe.api.OrderInfoService","createMillisTime":1622446261198,"generic":false,"invokeType":"sync","loadBalance":"random","methodName":"queryOrderInfo","parameterTypes":["com.lvyh.lightframe.api.dto.OrderInfoRequest"],"parameters":[{"userId":3000}],"requestId":"26c8fe822ed544278f63692d557a0a40","serverAddress":"192.168.253.1:7080","serviceKey":"com.lvyh.lightframe.api.OrderInfoService","timeout":2000,"transport":"nettyclient"}
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-877746557] INFO  - [TpsLimitFilter] start to execute tps limit filter.
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-1581662988] INFO  - [EchoFilter] start to execute echo filter.
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-877746557] INFO  - [EchoFilter] start to execute echo filter.
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-1581662988] INFO  - [GlobalTraceFilter] start to execute trace filter.
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-877746557] INFO  - [GlobalTraceFilter] start to execute trace filter.
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-1581662988] INFO  - queryOrderInfo request:OrderInfoRequest(userId=3000), result:OrderInfoResponse(orderId=111111111111, userId=3000, productId=500000, amount=260)
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-877746557] INFO  - queryOrderInfo request:OrderInfoRequest(userId=3000), result:OrderInfoResponse(orderId=111111111111, userId=3000, productId=500000, amount=260)
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-1581662988] INFO  - [MethodInvokeExecutor] provider complete service, request:{"attachment":{"traceId":"23640027-9505-430c-bce7-4414fb34debf"},"className":"com.lvyh.lightframe.api.OrderInfoService","createMillisTime":1622446261198,"generic":false,"invokeType":"sync","loadBalance":"random","methodName":"queryOrderInfo","parameterTypes":["com.lvyh.lightframe.api.dto.OrderInfoRequest"],"parameters":[{"userId":3000}],"requestId":"26c8fe822ed544278f63692d557a0a40","serverAddress":"192.168.253.1:7080","serviceKey":"com.lvyh.lightframe.api.OrderInfoService","timeout":2000,"transport":"nettyclient"},result:{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-1581662988] INFO  - [ProviderFilter] cluster execute return result:RpcResponse(requestId=26c8fe822ed544278f63692d557a0a40, result=OrderInfoResponse(orderId=111111111111, userId=3000, productId=500000, amount=260), isError=false, errorMsg=null, attachments=null)
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-877746557] INFO  - [MethodInvokeExecutor] provider complete service, request:{"attachment":{"traceId":"23640027-9505-430c-bce7-4414fb34debf"},"className":"com.lvyh.lightframe.api.OrderInfoService","createMillisTime":1622446261198,"generic":false,"invokeType":"sync","loadBalance":"random","methodName":"queryOrderInfo","parameterTypes":["com.lvyh.lightframe.api.dto.OrderInfoRequest"],"parameters":[{"userId":3000}],"requestId":"26c8fe822ed544278f63692d557a0a40","serverAddress":"192.168.253.1:7080","serviceKey":"com.lvyh.lightframe.api.OrderInfoService","timeout":2000,"transport":"nettyclient"},result:{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}
2021-05-31 15:31:04 [rpc, NettyServerBootstrap-ServerThreadPool-877746557] INFO  - [ProviderFilter] cluster execute return result:RpcResponse(requestId=26c8fe822ed544278f63692d557a0a40, result=OrderInfoResponse(orderId=111111111111, userId=3000, productId=500000, amount=260), isError=false, errorMsg=null, attachments=null)

```

客户端将打印：

```
2021-05-31 15:31:04 [http-nio-8081-exec-1] INFO  - [AbstractCluster] print invoke request id:26c8fe822ed544278f63692d557a0a40
2021-05-31 15:31:04 [http-nio-8081-exec-1] INFO  - [AbstractCluster] consumer start to invoke romote service, request:{"attachment":{"traceId":"23640027-9505-430c-bce7-4414fb34debf"},"className":"com.lvyh.lightframe.api.OrderInfoService","createMillisTime":1622446261198,"generic":false,"invokeType":"sync","loadBalance":"random","methodName":"queryOrderInfo","parameterTypes":["com.lvyh.lightframe.api.dto.OrderInfoRequest"],"parameters":[{"userId":3000}],"requestId":"26c8fe822ed544278f63692d557a0a40","serverAddress":"192.168.253.1:7080","serviceKey":"com.lvyh.lightframe.api.OrderInfoService","timeout":2000,"transport":"nettyclient"}
2021-05-31 15:31:04 [http-nio-8081-exec-1] INFO  - [RpcResponseFuture] consumer get response future,isDone：false,timeout:2000, response:null,currTime:2021-05-31 15:31:04.118
2021-05-31 15:31:04 [nioEventLoopGroup-2-1] INFO  - [NettyClientHandler] client receive remote result:{"error":false,"requestId":"26c8fe822ed544278f63692d557a0a40","result":{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}}
2021-05-31 15:31:04 [nioEventLoopGroup-2-1] INFO  - [RpcInvokeContext] consumer set response future:{"error":false,"requestId":"26c8fe822ed544278f63692d557a0a40","result":{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}},requestId:26c8fe822ed544278f63692d557a0a40
2021-05-31 15:31:04 [nioEventLoopGroup-2-1] INFO  - [NettyClientHandler] client refreshe invoke context response future:{"error":false,"requestId":"26c8fe822ed544278f63692d557a0a40","result":{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}}
2021-05-31 15:31:04 [http-nio-8081-exec-1] INFO  - [RpcResponseFuture] consumer get response future wait timeout,isDone：true,timeout:2000, response:{"error":false,"requestId":"26c8fe822ed544278f63692d557a0a40","result":{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}},currTime:2021-05-31 15:31:04.797
2021-05-31 15:31:04 [nioEventLoopGroup-2-1] INFO  - [NettyClientHandler] client receive remote result:{"error":false,"requestId":"26c8fe822ed544278f63692d557a0a40","result":{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}}
2021-05-31 15:31:04 [http-nio-8081-exec-1] INFO  - [AbstractCluster] consumer get response future, request:{"error":false,"requestId":"26c8fe822ed544278f63692d557a0a40","result":{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}}
2021-05-31 15:31:04 [nioEventLoopGroup-2-1] INFO  - [RpcInvokeContext] consumer set null response future ,requestId:26c8fe822ed544278f63692d557a0a40
2021-05-31 15:31:04 [nioEventLoopGroup-2-1] INFO  - [NettyClientHandler] client refreshe invoke context response future:{"error":false,"requestId":"26c8fe822ed544278f63692d557a0a40","result":{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}}
2021-05-31 15:31:04 [http-nio-8081-exec-1] INFO  - [FailoverCluster] consumer cluster execute invoke romote service,time:1, request:{"attachment":{"traceId":"23640027-9505-430c-bce7-4414fb34debf"},"className":"com.lvyh.lightframe.api.OrderInfoService","createMillisTime":1622446261198,"generic":false,"invokeType":"sync","loadBalance":"random","methodName":"queryOrderInfo","parameterTypes":["com.lvyh.lightframe.api.dto.OrderInfoRequest"],"parameters":[{"userId":3000}],"requestId":"26c8fe822ed544278f63692d557a0a40","serverAddress":"192.168.253.1:7080","serviceKey":"com.lvyh.lightframe.api.OrderInfoService","timeout":2000,"transport":"nettyclient"}, response:{"error":false,"requestId":"26c8fe822ed544278f63692d557a0a40","result":{"amount":"260","orderId":"111111111111","productId":500000,"userId":3000}}
2021-05-31 15:31:04 [http-nio-8081-exec-1] ERROR - [FailoverCluster] consumer cluster execute invoke error,time:1, className:class com.lvyh.lightframe.common.exception.RpcRuntimeException, message:invoke error,,provider:[192.168.253.1:7080]
2021-05-31 15:31:04 [http-nio-8081-exec-1] INFO  - [ConsumerFilter] cluster execute return result:RpcResponse(requestId=26c8fe822ed544278f63692d557a0a40, result=OrderInfoResponse(orderId=111111111111, userId=3000, productId=500000, amount=260), isError=false, errorMsg=null, attachments=null)

```
