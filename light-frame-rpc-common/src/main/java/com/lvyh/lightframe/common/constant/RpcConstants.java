/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.common.constant;

import java.util.regex.Pattern;

/**
 * @author lvyh 2021/05/09.
 */
public class RpcConstants {

    public static int ZK_SESSION_TIMEOUT = 5000;

    public static int ZK_CONNECTION_TIMEOUT = 2000;

    public static final String ADDRESS_DEFAULT_GROUP = "_DEFAULT";

    public static final String ADDRESS_DIRECT_GROUP = "_DIRECT";

    /**
     * invoke num
     */
    public static final String INTERNAL_KEY_INVOKE_TIMES = "_invoke_times";

    public static String ZK_REGISTRY_PATH = "/registry";

    public static final String REGISTER_SERVICE_NAME = "uniqueMetaName";

    /**
     * Path of extension point load
     */
    public static final String EXTENSION_LOAD_PATH = "META-INF/rpc/";

    public static final String LOADBALANCE_RANDOM = "random";

    public static final String LOADBALANCE_LRU = "lru";

    public static final String LOADBALANCE_LFU = "lfu";

    public static final String LOADBALANCE_ROUND = "round";

    public static final String LOADBALANCE_CONSISTENT = "consistent";

    public static final String TRANSPORT_NETTY = "nettyserver";

    public static final String TRANSPORT_HTTP = "httpserver";

    public static final String TRANSPORT_CLIENT_NETTY = "nettyclient";

    public static final String TRANSPORT_CLIENT_HTTP = "httpclient";

    /**
     * Zookeeper registry
     */
    public static final String REGISTRY_TYPE_ZK = "zk";

    /**
     * etcd registry
     */
    public static final String REGISTRY_TYPE_ETCD = "etcd";

    /**
     * nacos registry
     */
    public static final String REGISTRY_TYPE_NACOS = "nacos";

    /**
     * Call mode: sync call
     */
    public static final String INVOKER_TYPE_SYNC = "sync";

    /**
     * Call mode: oneway call
     */
    public static final String INVOKER_TYPE_ONEWAY = "oneway";

    /**
     * Call mode: callback call
     */
    public static final String INVOKER_TYPE_CALLBACK = "callback";

    /**
     * Call mode: future call
     */
    public static final String INVOKER_TYPE_FUTURE = "future";

    /**
     * Cluster fault tolerance: broadcast
     */
    public static final String CLUSTER_BROADCAST = "broadcast";

    /**
     * Cluster fault tolerance: failback
     */
    public static final String CLUSTER_FAILBACK = "failback";

    /**
     * Cluster fault tolerance: failfast
     */
    public static final String CLUSTER_FAILFAST = "failfast";

    /**
     * Cluster fault tolerance: failover
     */
    public static final String CLUSTER_FAILOVER = "failover";


    /**
     * Cluster fault tolerance: failsafe
     */
    public static final String CLUSTER_FAILSAFE = "failsafe";

    /**
     * Hessian serialization
     */
    public static final String SERIALIZE_HESSIAN = "hessian";
    /**
     * Hessian2 serialization
     */
    public static final String SERIALIZE_HESSIAN2 = "hessian2";
    /**
     * Java serialization
     */
    public static final String SERIALIZE_JAVA = "java";
    /**
     * protobuf serialization
     */
    public static final String SERIALIZE_PROTOBUF = "protobuf";
    /**
     * json serialization
     */
    public static final String SERIALIZE_JSON = "json";

    public static final String GROUP_KEY = "group";

    public static final String PATH_KEY = "path";

    public static final String INTERFACE_KEY = "interface";

    public static final String VERSION_KEY = "version";

    public static final String DEFAULT_KEY_PREFIX = "default.";

    public static final String DEFAULT_KEY = "default";

    public static final Pattern COMMA_SPLIT_PATTERN = Pattern.compile("\\s*[,]+\\s*");

    public static final String $ECHO = "$echo";

    public static final int DEFAULT_TPS_LIMIT_RATE = 1000;

    public static final long DEFAULT_TPS_LIMIT_INTERVAL = 60 * 1000;

    public static final long DEFAULT_INVOKE_TIMEOUT = 1000;

    public static final String DEFAULT_GROUP_NAME = "default";

    public static final String INVOKE_FILTER_CUSTOMER = "consumer";

    public static final String INVOKE_FILTER_PROVIDER = "provider";
}