/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.config;

import com.lvyh.lightframe.common.constant.RpcConstants;
import com.lvyh.lightframe.common.RpcContext;
import com.lvyh.lightframe.core.invoke.response.ResponseCallback;
import com.lvyh.lightframe.common.util.IpUtil;
import com.lvyh.lightframe.core.util.SpringUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.List;

/**
 * @author lvyh 2021/05/16.
 */
@Data
@Slf4j
public class ConsumerConfig implements ApplicationContextAware, InitializingBean {

    private String id;

    private String name;

    private String ip;

    private int port;

    private int weight;

    private int timeout;

    private String version;
    /**
     * Direct link call address
     */
    private String directUrl;

    /**
     * generic invoke
     */
    private boolean generic;

    private int connectTimeout;

    private String loadBalance;

    private String serializer;

    private String transport = RpcConstants.TRANSPORT_CLIENT_NETTY;

    private String invokeType = RpcConstants.INVOKER_TYPE_FUTURE;

    private String cluster = RpcConstants.CLUSTER_FAILOVER;

    private ResponseCallback responseCallback = null;

    /**
     * Filter configuration alias, separated by commas
     */
    protected List<String> filter;


    public ConsumerConfig() {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringUtil.setApplicationContext(applicationContext);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        RpcContext.setApplicationName(name);
        RpcContext.setPort(port);
        RpcContext.setSerializer(serializer);
        RpcContext.setLocalIp(IpUtil.getIpAddress());
        RpcContext.setCluster(cluster);
        RpcContext.setFilter(filter);
        RpcContext.setLoadBalance(loadBalance);
    }


    public ConsumerConfig setId(String id) {
        this.id = id;
        return this;
    }

    public ConsumerConfig setName(String name) {
        this.name = name;
        return this;
    }

    public ConsumerConfig setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public ConsumerConfig setPort(int port) {
        this.port = port;
        return this;
    }

    public ConsumerConfig setWeight(int weight) {
        this.weight = weight;
        return this;
    }

    public ConsumerConfig setDirectUrl(String directUrl) {
        this.directUrl = directUrl;
        return this;
    }

    public ConsumerConfig setGeneric(boolean generic) {
        this.generic = generic;
        return this;
    }

    public ConsumerConfig setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
        return this;
    }

    public ConsumerConfig setLoadBalance(String loadBalance) {
        this.loadBalance = loadBalance;
        return this;
    }

    public ConsumerConfig setSerializer(String serializer) {
        this.serializer = serializer;
        return this;
    }

    public ConsumerConfig setTransport(String transport) {
        this.transport = transport;
        return this;
    }

    public ConsumerConfig setInvokeType(String invokeType) {
        this.invokeType = invokeType;
        return this;
    }

    public ConsumerConfig setCluster(String cluster) {
        this.cluster = cluster;
        return this;
    }

    public ConsumerConfig setResponseCallback(ResponseCallback responseCallback) {
        this.responseCallback = responseCallback;
        return this;
    }

    public ConsumerConfig setFilter(List<String> filter) {
        this.filter = filter;
        return this;
    }


}
