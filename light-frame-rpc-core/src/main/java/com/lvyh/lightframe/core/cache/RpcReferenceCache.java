/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.cache;

import com.lvyh.lightframe.core.consumer.reference.ReferenceModel;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author lvyh 2021/05/16.
 */
public class RpcReferenceCache {
    private static ConcurrentMap<String, ReferenceModel> referenceCache = new ConcurrentHashMap<String, ReferenceModel>();

    public static void put(String key, ReferenceModel rpcReferenceBean) {
        referenceCache.putIfAbsent(key, rpcReferenceBean);
    }

    public static ReferenceModel get(String key) {
        return referenceCache.get(key);
    }
}
