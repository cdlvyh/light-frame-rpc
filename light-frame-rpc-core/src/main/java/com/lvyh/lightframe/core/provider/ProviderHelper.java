/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.provider;

import com.lvyh.lightframe.common.util.IpUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lvyh 2021/05/22.
 */
public class ProviderHelper {

    public static final List<String> convert(List<ProviderInfo> addressList) {
        List<String> result = new ArrayList<>();
        for (ProviderInfo bean : addressList) {
            result.add(bean.getHost() + ":" + bean.getPort());
        }
        return result;
    }

    public static final List<ProviderInfo> convertToProviderList(List<String> addressList) {
        List<ProviderInfo> result = new ArrayList<>();
        for (String address : addressList) {
            Object[] array = IpUtil.parseIpPort(address);
            String host = (String) array[0];
            int port = (int) array[1];
            ProviderInfo bean = new ProviderInfo(host, port);
            result.add(bean);
        }
        return result;
    }

    public static final String convert(ProviderInfo bean) {
        return bean.getHost() + ":" + bean.getPort();
    }

}
