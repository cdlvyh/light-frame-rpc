/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.serialize.impl;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.lvyh.lightframe.common.ext.Spi;
import com.lvyh.lightframe.core.serialize.Serializer;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Spi("hessian")
public class HessianSerializer extends Serializer {

    private static HessianSerializer instance = new HessianSerializer();

    private HessianSerializer() {
    }

    public static HessianSerializer getInstance() {
        return instance;
    }

    @Override
    public <T> byte[] serialize(T obj) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Hessian2Output ho = new Hessian2Output(os);
        try {
            ho.writeObject(obj);
            ho.flush();
            byte[] result = os.toByteArray();
            return result;
        } catch (IOException e) {
            throw new RpcRuntimeException(e);
        } finally {
            try {
                ho.close();
            } catch (IOException e) {
                throw new RpcRuntimeException(e);
            }
            try {
                os.close();
            } catch (IOException e) {
                throw new RpcRuntimeException(e);
            }
        }

    }

    @Override
    public <T> Object deserialize(byte[] bytes, Class<T> clazz) {
        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        Hessian2Input hi = new Hessian2Input(is);
        try {
            Object result = hi.readObject();
            return result;
        } catch (IOException e) {
            throw new RpcRuntimeException(e);
        } finally {
            try {
                hi.close();
            } catch (Exception e) {
                throw new RpcRuntimeException(e);
            }
            try {
                is.close();
            } catch (IOException e) {
                throw new RpcRuntimeException(e);
            }
        }
    }

}
