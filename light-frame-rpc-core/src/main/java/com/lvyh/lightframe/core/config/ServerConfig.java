/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.config;


import com.lvyh.lightframe.common.RpcContext;
import com.lvyh.lightframe.common.util.IpUtil;
import com.lvyh.lightframe.core.util.SpringUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.List;

/**
 * @author lvyh 2021/05/16.
 */
@Data
@Slf4j
public class ServerConfig implements ApplicationContextAware, InitializingBean {
    private String id;

    private String name;

    private String ip;

    private int port;

    private int weight;

    private String serializer = null;

    private String transport;

    /**
     * Filter configuration alias, separated by commas
     */
    protected List<String> filter;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringUtil.setApplicationContext(applicationContext);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        RpcContext.setApplicationName(name);
        RpcContext.setPort(port);
        RpcContext.setSerializer(serializer);
        RpcContext.setLocalIp(IpUtil.getIpAddress());
        RpcContext.setTransport(transport);
        RpcContext.setFilter(filter);
    }

}
