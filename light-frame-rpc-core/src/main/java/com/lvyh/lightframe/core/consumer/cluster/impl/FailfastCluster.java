/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.consumer.cluster.impl;

import com.lvyh.lightframe.core.provider.ProviderHelper;
import com.lvyh.lightframe.core.provider.ProviderInfo;
import com.lvyh.lightframe.core.consumer.cluster.AbstractCluster;
import com.lvyh.lightframe.core.consumer.route.LoadBalance;
import com.lvyh.lightframe.core.consumer.transport.AbstractClient;
import com.lvyh.lightframe.common.ext.ExtensionLoaderFactory;
import com.lvyh.lightframe.common.ext.Spi;
import com.lvyh.lightframe.core.invoke.request.RpcRequest;
import com.lvyh.lightframe.core.invoke.response.RpcResponse;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Fast failure, only one call is initiated, and an error is reported immediately if it fails.
 * It is usually used for non idempotent write operations, such as adding new records
 */
@Spi("failfast")
public class FailfastCluster extends AbstractCluster {
    private Logger logger = LoggerFactory.getLogger(FailfastCluster.class);

    public FailfastCluster(LoadBalance loadBalance) {
        super(loadBalance);
    }

    @Override
    public RpcResponse doInvoke(RpcRequest request) throws RpcRuntimeException {
        ProviderInfo providerInfo = select(request, loadBalance);
        request.setServerAddress(ProviderHelper.convert(providerInfo));
        try {
            AbstractClient client = ExtensionLoaderFactory.getExtensionLoader(AbstractClient.class).getExtension(request.getTransport());
            RpcResponse response = doInvoke(client, request);
            if (response != null) {
                return response;
            } else {
                throw new RpcRuntimeException("Failed to call " + request.getClassName() + "." + request.getMethodName()
                        + " on remote server " + providerInfo + ", return null");
            }
        } catch (Exception e) {
            throw new RpcRuntimeException("Failed to call " + request.getClassName() + "." + request.getMethodName()
                    + " on remote server: " + providerInfo + ", cause by: "
                    + e.getClass().getName() + ", message is: " + e.getMessage(), e);
        }
    }


}
