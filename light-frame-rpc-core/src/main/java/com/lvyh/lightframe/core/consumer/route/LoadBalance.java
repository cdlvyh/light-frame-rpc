/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.consumer.route;

import com.lvyh.lightframe.common.util.CommonUtils;
import com.lvyh.lightframe.core.provider.ProviderInfo;
import com.lvyh.lightframe.common.ext.Extensible;
import com.lvyh.lightframe.core.invoke.request.RpcRequest;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;

import javax.annotation.concurrent.ThreadSafe;
import java.util.List;

/**
 * @author lvyh 2021/05/09.
 */
@Extensible
@ThreadSafe
public abstract class LoadBalance {

    public ProviderInfo select(RpcRequest request, List<ProviderInfo> providerInfos) throws RpcRuntimeException {
        String targetServiceUniqueName = CommonUtils.buildServiceUniqueCode(request.getClassName(), request.getVersion());
        if (providerInfos.size() == 0) {
            throw new RpcRuntimeException("can not get rpc provider, current service:" + targetServiceUniqueName);
        }
        if (providerInfos.size() == 1) {
            return providerInfos.get(0);
        } else {
            return doSelect(request, providerInfos);
        }
    }

    /**
     * Filter according to load balance rules
     */
    protected abstract ProviderInfo doSelect(RpcRequest rpcRequest, List<ProviderInfo> providerInfoList);

}
