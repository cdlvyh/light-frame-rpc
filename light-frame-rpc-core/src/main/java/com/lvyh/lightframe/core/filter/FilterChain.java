/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.filter;

import com.lvyh.lightframe.core.invoke.request.RpcRequest;
import com.lvyh.lightframe.core.invoke.response.RpcResponse;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;

/**
 * @author lvyh 2021/05/22.
 */
public class FilterChain {
    private AbstractFilter head = null;
    private AbstractFilter tail = null;

    public void addHandler(AbstractFilter handler) {
        if (head == null) {
            head = tail = handler;
        }

        /** Set current tail successor **/
        tail.setNextFilter(handler);

        /** The pointer points to tail again **/
        tail = handler;
    }

    public RpcResponse filter(RpcRequest request) {
        if (null == head) {
            throw new RpcRuntimeException("FilterChain is not empty");
        }

        /** head Initiate the call **/
        return head.filter(request);
    }
}
