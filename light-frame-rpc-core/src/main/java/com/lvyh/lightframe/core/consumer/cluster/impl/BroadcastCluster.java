/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.consumer.cluster.impl;

import com.lvyh.lightframe.core.provider.ProviderHelper;
import com.lvyh.lightframe.core.provider.ProviderInfo;
import com.lvyh.lightframe.core.consumer.cluster.AbstractCluster;
import com.lvyh.lightframe.core.consumer.route.LoadBalance;
import com.lvyh.lightframe.core.consumer.transport.AbstractClient;
import com.lvyh.lightframe.common.ext.ExtensionLoaderFactory;
import com.lvyh.lightframe.common.ext.Spi;
import com.lvyh.lightframe.core.invoke.request.RpcRequest;
import com.lvyh.lightframe.core.invoke.response.RpcResponse;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Broadcast notify message
 */
@Spi("broadcast")
public class BroadcastCluster extends AbstractCluster {
    private Logger logger = LoggerFactory.getLogger(BroadcastCluster.class);
    RpcRuntimeException exception = null;

    public BroadcastCluster(LoadBalance loadBalance) {
        super(loadBalance);
    }

    @Override
    public RpcResponse doInvoke(RpcRequest request) throws RpcRuntimeException {
        RpcResponse response = null;
        List<ProviderInfo> providerInfoList = select(request);
        for (ProviderInfo providerInfo : providerInfoList) {
            try {
                request.setServerAddress(ProviderHelper.convert(providerInfo));
                AbstractClient client = ExtensionLoaderFactory.getExtensionLoader(AbstractClient.class).getExtension(request.getTransport());
                response = doInvoke(client, request);
            } catch (Exception e) {
                exception = new RpcRuntimeException(e.getMessage(), e);
                logger.error(e.getMessage(), e);
            }
        }
        if (exception != null) {
            throw exception;
        }
        return response;
    }
}
