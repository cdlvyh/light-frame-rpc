/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.filter.impl;


import com.lvyh.lightframe.core.config.ReferenceConfig;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;
import com.lvyh.lightframe.common.ext.Spi;
import com.lvyh.lightframe.core.filter.AbstractFilter;
import com.lvyh.lightframe.core.generic.GenericService;
import com.lvyh.lightframe.core.invoke.request.RpcRequest;
import com.lvyh.lightframe.core.invoke.response.RpcResponse;
import com.lvyh.lightframe.common.util.CommonIdUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * client generic invoke filter
 */
@Spi("consumerGeneric")
public class ConsumerGenericFilter extends AbstractFilter {
    private Logger logger = LoggerFactory.getLogger(ConsumerGenericFilter.class);


    @Override
    public RpcResponse doFilter(RpcRequest request) throws RpcRuntimeException {

        try {
            ReferenceConfig referenceConfig = request.getReferenceConfig();
            String className = request.getClassName();
            String methodName = request.getMethodName();
            Object[] args = request.getParameters();

            Object[] parameters = null;
            Class<?>[] parameterTypes = null;
            String[] parameterTypesValue = null;

            // Correction request object
            //Object $invoke(String methodName, String[] argTypes, Object[] args);
            if (className.equals(GenericService.class.getName()) && methodName.equals("$invoke")) {
                Class<?>[] paramTypes = null;
                if (args[1] != null) {
                    parameterTypesValue = (String[]) args[1];
                }
                methodName = (String) args[0];
                parameterTypes = paramTypes;
                parameters = (Object[]) args[2];
                className = referenceConfig.getInterfaceId();

                request.setGeneric(true);
                request.setClassName(className);
                request.setMethodName(methodName);
                request.setParameterTypes(parameterTypes);
                request.setParameters(parameters);
                request.setRequestId(CommonIdUtil.generateSimpleUUID());
                request.setParameterTypesValue(parameterTypesValue);
            }
            logger.info("[ConsumerGenericFilter] start to execute consumer generic filter, request:{}", request);
            return null;
        } catch (RpcRuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RpcRuntimeException(e.getMessage(), e);
        }
    }


}
