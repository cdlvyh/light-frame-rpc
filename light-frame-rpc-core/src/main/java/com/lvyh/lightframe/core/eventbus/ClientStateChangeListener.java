/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.eventbus;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lvyh 2021/05/15.
 */
@EventBusListener
@Slf4j
public class ClientStateChangeListener {

    @Subscribe
    public void start(ClientStartedEvent event) throws InterruptedException {
        log.info("Client started, serverConfig:{}", event.getServerConfig());
    }

    @Subscribe
    public void stop(ClientStoppedEvent event) throws InterruptedException {
        log.info(" Client stopped , serverConfig:{}", event.getServerConfig());
    }
}
