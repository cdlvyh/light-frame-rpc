/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.consumer.transport;

import com.lvyh.lightframe.common.ext.Extensible;
import com.lvyh.lightframe.core.invoke.RpcInvokeContext;
import com.lvyh.lightframe.core.invoke.request.RpcRequest;
import com.lvyh.lightframe.core.util.SpringContextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author lvyh 2021/05/15.
 */
@Extensible
@ThreadSafe
public abstract class AbstractClient {
    protected static final Logger logger = LoggerFactory.getLogger(AbstractClient.class);

    public abstract void send(RpcRequest rpcRequest) throws Exception;

    private static volatile ConcurrentMap<String, ConnectClient> connectClientCache = new ConcurrentHashMap<String, ConnectClient>();

    public ConnectClient getConnectClient(RpcRequest request, Class<? extends ConnectClient> connectClient) throws Exception {

        String serverAddress = request.getServerAddress();
        ConnectClient instance = connectClientCache.get(serverAddress);
        if (instance != null && instance.isValidate()) {
            return instance;
        }
        RpcInvokeContext rpcInvoker = SpringContextUtils.getBean(RpcInvokeContext.class);
        try {
            instance = ConnectTransportFactory.getInstance(connectClient);
            instance.init(serverAddress, rpcInvoker);
            connectClientCache.putIfAbsent(serverAddress, instance);
        } catch (Exception e) {
            instance.close();
            throw e;
        }

        return instance;
    }

}
