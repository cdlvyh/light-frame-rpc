/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.filter.impl;

import com.lvyh.lightframe.common.RpcContext;
import com.lvyh.lightframe.core.consumer.cluster.Cluster;
import com.lvyh.lightframe.core.filter.AbstractFilter;
import com.lvyh.lightframe.core.consumer.route.LoadBalance;
import com.lvyh.lightframe.common.ext.ExtensionLoaderFactory;
import com.lvyh.lightframe.common.ext.Spi;
import com.lvyh.lightframe.core.invoke.request.RpcRequest;
import com.lvyh.lightframe.core.invoke.response.RpcResponse;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author lvyh 2021/05/23.
 */
@Spi("consumer")
public class ConsumerFilter extends AbstractFilter {
    private Logger logger = LoggerFactory.getLogger(ConsumerFilter.class);

    @Override
    public RpcResponse doFilter(RpcRequest request) throws RpcRuntimeException {
        LoadBalance loadBalance = ExtensionLoaderFactory.getExtensionLoader(LoadBalance.class).getExtension(request.getLoadBalance());
        Cluster cluster = ExtensionLoaderFactory.getExtensionLoader(Cluster.class).getExtension(RpcContext.getCluster(), new Class[]{LoadBalance.class}, new Object[]{loadBalance});
        RpcResponse response = cluster.invoke(request);

        logger.info("[ConsumerFilter] cluster execute return result:{}", response);
        return response;
    }


}
