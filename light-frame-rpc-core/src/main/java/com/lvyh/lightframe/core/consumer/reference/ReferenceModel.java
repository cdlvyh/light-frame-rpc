/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.consumer.reference;

import com.lvyh.lightframe.core.invoke.response.ResponseCallback;
import com.lvyh.lightframe.core.serialize.Serializer;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author lvyh 2021/05/09.
 */
@Data
public class ReferenceModel {
    private static final Logger logger = LoggerFactory.getLogger(ReferenceModel.class);

    private String interfaceName = null;

    private String version = null;

    private long timeout = 10000;

    private String invokeType = null;

    private String address = null;

    private ResponseCallback responseCallback = null;

    private String transport = null;

    private String loadBalance = null;

    private Serializer serializer = null;

    private String cluster = null;

    public static class Builder {
        private ReferenceModel instance;

        public Builder(ReferenceModel q) {
            this.instance = q;
        }

        public Builder() {
            this(new ReferenceModel());
        }

        public Builder setInterfaceName(String interfaceName) {
            this.instance.interfaceName = interfaceName;
            return this;
        }

        public Builder setVersion(String version) {
            this.instance.version = version;
            return this;
        }

        public Builder setTimeout(long timeout) {
            this.instance.timeout = timeout;
            return this;
        }

        public Builder setInvokeType(String invokeType) {
            this.instance.invokeType = invokeType;
            return this;
        }

        public Builder setAddress(String address) {
            this.instance.address = address;
            return this;
        }

        public Builder setResponseCallback(ResponseCallback responseCallback) {
            this.instance.responseCallback = responseCallback;
            return this;
        }

        public Builder setTransport(String transport) {
            this.instance.transport = transport;
            return this;
        }

        public Builder setLoadBalance(String loadBalance) {
            this.instance.loadBalance = loadBalance;
            return this;
        }

        public Builder setSerializer(Serializer serializer) {
            this.instance.serializer = serializer;
            return this;
        }

        public Builder setCluster(String cluster) {
            this.instance.cluster = cluster;
            return this;
        }

        public ReferenceModel build() {
            return this.instance;
        }
    }

}
