/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.consumer.proxy.impl;


import com.lvyh.lightframe.core.config.ReferenceConfig;
import com.lvyh.lightframe.core.consumer.proxy.RpcProxyFactory;
import com.lvyh.lightframe.core.consumer.proxy.interceptor.CallbackInterceptor;
import org.springframework.cglib.proxy.Enhancer;

/**
 * @author lvyh 2021/05/16.
 */
public class CGLIBProxyFactory implements RpcProxyFactory {

    @Override
    public Object getProxy(Class<?> clazz, ReferenceConfig referenceConfig) {

        Enhancer enhancer = new Enhancer();
        //Set proxy target class
        enhancer.setSuperclass(clazz);

        //Set up the interceptor, which is enhanced in this interceptor
        enhancer.setCallback(new CallbackInterceptor(clazz, referenceConfig));

        return enhancer.create();
    }
}
