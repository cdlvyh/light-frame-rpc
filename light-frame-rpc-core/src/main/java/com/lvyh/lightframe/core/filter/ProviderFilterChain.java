/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.filter;

import com.lvyh.lightframe.common.constant.RpcConstants;
import com.lvyh.lightframe.common.RpcContext;
import com.lvyh.lightframe.common.ext.ExtensionLoader;
import com.lvyh.lightframe.common.ext.ExtensionLoaderFactory;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author lvyh 2021/05/23.
 */
public class ProviderFilterChain extends FilterChain {

    @PostConstruct
    public void init() {
        ExtensionLoader<Filter> extensionLoader = ExtensionLoaderFactory.getExtensionLoader(Filter.class);
        List<String> aliasList = RpcContext.getFilter();
        for (String alias : aliasList) {
            AbstractFilter filter = (AbstractFilter) extensionLoader.getExtension(alias);
            addHandler(filter);
        }

        //Forced injection of provider filter at the end of linked list
        if (!aliasList.contains(RpcConstants.INVOKE_FILTER_PROVIDER)) {
            AbstractFilter filter = (AbstractFilter) extensionLoader.getExtension(RpcConstants.INVOKE_FILTER_PROVIDER);
            addHandler(filter);
        }
    }
}
