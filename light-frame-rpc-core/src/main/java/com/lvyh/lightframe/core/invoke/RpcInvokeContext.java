/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.invoke;

import com.alibaba.fastjson.JSON;
import com.lvyh.lightframe.core.invoke.response.ResponseCallback;
import com.lvyh.lightframe.core.invoke.response.RpcResponseFuture;
import com.lvyh.lightframe.core.invoke.response.RpcResponse;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;
import com.lvyh.lightframe.common.util.ThreadPoolUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ThreadPoolExecutor;
/**
 * @author lvyh 2021/05/23.
 */
public class RpcInvokeContext {
    private static Logger logger = LoggerFactory.getLogger(RpcInvokeContext.class);


    public RpcInvokeContext() {
    }

    private ThreadPoolExecutor threadPoolExecutor = ThreadPoolUtils.makeServerThreadPool(RpcInvokeContext.class.getSimpleName(), 60, 300);
    private ConcurrentMap<String, RpcResponseFuture> responseFutureCache = new ConcurrentHashMap<String, RpcResponseFuture>();

    public void setResponseFuture(String requestId, RpcResponseFuture responseFuture) {
        responseFutureCache.putIfAbsent(requestId, responseFuture);
    }

    public void removeResponseFuture(String requestId) {
        responseFutureCache.remove(requestId);
    }

    public void setInvokeResponseFuture(String requestId, final RpcResponse rpcResponse) {

        RpcResponseFuture responseFuture = responseFutureCache.get(requestId);
        if (responseFuture == null) {
            logger.info("[RpcInvokeContext] consumer set null response future ,requestId:{}", requestId);
            return;
        }

        if (responseFuture.getResponseCallback() != null) {
            responseFuture.setResponse(rpcResponse);
            processResponseCallback(responseFuture);
            logger.info("[RpcInvokeContext] consumer set response future:{},requestId:{}", JSON.toJSONString(rpcResponse), requestId);
        } else {
            responseFuture.setResponse(rpcResponse);
            logger.info("[RpcInvokeContext] consumer set response future:{},requestId:{}", JSON.toJSONString(rpcResponse), requestId);
        }

        responseFutureCache.remove(requestId);
    }

    private void processResponseCallback(final RpcResponseFuture futureResponse) {
        try {
            ResponseCallback responseCallback = futureResponse.getResponseCallback();
            RpcResponse rpcResponse = futureResponse.getResponse();
            logger.info("[ResponseCallback] process response callback,rpcResponse:{}", rpcResponse);

            threadPoolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    if (rpcResponse.getErrorMsg() != null) {
                        responseCallback.onException(new RpcRuntimeException(rpcResponse.getErrorMsg()));
                    } else {
                        responseCallback.onSuccess(rpcResponse.getResult());
                    }
                }
            });
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void destroy() throws Exception {
        if (threadPoolExecutor != null) {
            threadPoolExecutor.shutdown();
        }
    }

}
