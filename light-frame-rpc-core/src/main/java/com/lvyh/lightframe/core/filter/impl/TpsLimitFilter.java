/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.core.filter.impl;

import com.lvyh.lightframe.core.filter.AbstractFilter;
import com.lvyh.lightframe.core.provider.tps.DefaultTPSLimiter;
import com.lvyh.lightframe.core.provider.tps.TPSLimiter;
import com.lvyh.lightframe.common.ext.Spi;
import com.lvyh.lightframe.core.invoke.request.RpcRequest;
import com.lvyh.lightframe.core.invoke.response.RpcResponse;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lvyh 2021/05/22.
 */
@Spi("tps")
public class TpsLimitFilter extends AbstractFilter {
    private Logger logger = LoggerFactory.getLogger(TpsLimitFilter.class);
    private final TPSLimiter tpsLimiter = new DefaultTPSLimiter();

    public RpcResponse doFilter(RpcRequest request) throws RpcRuntimeException {

        logger.info("[TpsLimitFilter] start to execute tps limit filter.");
        if (!tpsLimiter.isAllowable(request)) {
            throw new RpcRuntimeException(
                    new StringBuilder(64)
                            .append("Failed to invoke service ")
                            .append(request.getClassName())
                            .append(".")
                            .append(request.getMethodName())
                            .append(" because exceed max service tps.")
                            .toString());
        }

        return null;
    }

}
