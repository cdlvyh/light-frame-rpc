/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.registry.nacos;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;

import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
/**
 * @author lvyh 2021/05/23.
 */
public class NacosHolder {

    private NacosHolder() {

    }

    private static NamingService namingService;
    private static final Lock LOCK = new ReentrantLock();

    public static NamingService getNaming(String address) throws NacosException {
        if (Objects.nonNull(namingService)) {
            return namingService;
        }

        LOCK.lock();
        try {
            if (Objects.nonNull(namingService)) {
                return namingService;
            }

            namingService = NamingFactory.createNamingService(address);
            return namingService;
        } finally {
            LOCK.unlock();
        }
    }
}
