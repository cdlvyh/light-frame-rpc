/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.registry.nacos;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.alibaba.nacos.client.naming.utils.NetUtils;
import com.lvyh.lightframe.common.RpcContext;
import com.lvyh.lightframe.common.constant.RpcConstants;
import com.lvyh.lightframe.common.exception.RpcRuntimeException;
import com.lvyh.lightframe.common.ext.Spi;
import com.lvyh.lightframe.registry.Registry;

import java.util.List;

/**
 * @author lvyh 2021/05/22.
 */
@Spi("nacos")
public class NacosRegistry implements Registry {

    @Override
    public void register(String serviceName, String serviceAddress) {
        Instance instance = new Instance();
        instance.setIp(NetUtils.localIP());
        instance.setPort(RpcContext.getPort());
        instance.addMetadata(RpcConstants.REGISTER_SERVICE_NAME, serviceName);

        try {
            NamingService naming = NacosHolder.getNaming(serviceAddress);
            naming.registerInstance(serviceName, instance);
        } catch (NacosException ex) {
            throw new RpcRuntimeException("Service registry failed", ex);
        }
    }

    @Override
    public List<String> discover(String name) {
        //TODO
        return null;
    }
}
