/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lvyh.lightframe.registry;

import com.lvyh.lightframe.common.ext.Extensible;

import javax.annotation.concurrent.ThreadSafe;
import java.util.List;

/**
 * Service register interface
 *
 * @author lvyh 2021/05/23.
 */
@Extensible
@ThreadSafe
public interface Registry {

    /**
     * Registry service name and address
     *
     * @param serviceName
     * @param serviceAddress
     */
    void register(String serviceName, String serviceAddress);

    /**
     * Find service address by service name
     *
     * @param serviceName
     * @return serviceAddress
     */
    List<String> discover(String serviceName);
}